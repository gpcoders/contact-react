import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import escapeRegExp from 'escape-string-regexp'
import sortBy from 'sort-by'


class ListContacts extends Component {

    state = {
        query: ''
    }

    static propTypes = {
        contacts: PropTypes.array.isRequired,
        onRemoveContact: PropTypes.func.isRequired
    }

    updateQuery = (query) => {
        this.setState({ query: query.trim() })
    }

    clearQuery = () => {
        this.setState({
            query: ''
        })
    }

    render() {

        const { contacts, onRemoveContact } = this.props
        const { query } = this.state

        let showingContacts
        if (this.state.query) {
            const match = new RegExp(escapeRegExp(query), 'i')
            showingContacts = contacts.filter((contact) => match.test(contact.name))
        } else {
            showingContacts = contacts
        }

        showingContacts.sort(sortBy('name'))

        return (
            <div className="list-contacts">
                <div className="list-contacts-top">
                    <input type="text" className="search-contacts"
                           placeholder="Search contacts"
                           value={query}
                           onChange={(event) => this.updateQuery(event.target.value)}
                    />
                    <Link to="/create" className="add-contact">Add Contact</Link>
                </div>

                {showingContacts.length !== contacts.length && (
                    <div className="showing-contacts">
                        <span>Now showing {showingContacts.length} of {contacts.length} </span>
                        <button onClick={this.clearQuery}>Show All</button>
                    </div>
                )}

                <ol className="contact-list">
                    {showingContacts.map((contact) => (
                        <li className="contact-list-item" key={contact.id}>
                            <div className="contact-avatar" style={{
                                backgroundImage: `url(${contact.avatarURL})`
                            }} />
                            <div className="contact-details">
                                <p>{contact.name}</p>
                                <p>{contact.email}</p>

                            </div>
                            <button className="contact-remove" onClick={() => onRemoveContact(contact)}>Remove</button>
                        </li>
                    ))}
                </ol>
            </div>
        )
    }

}




export default ListContacts